<?php namespace Monologophobia\Drive\Models;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use League\Flysystem\AdapterInterface;

use Response;
use BackendAuth;
use \October\Rain\Database\Model;

use Monologophobia\Drive\Models\DriveSettings as Settings;

class Drive extends Model {

    public $table = 'mono_company_drive';
    public $timestamps = true;

    private $filesystem;

    protected $nullable = ['mime', 'thumbnail'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'type' => 'required|string',
        'name' => 'required|string'
    ];

    public $belongsTo =[
        'owner' => ['Backend\Models\User', 'key' => 'owner_id']
    ];

    public $belongsToMany = [
        'users' => [
            'Backend\Models\User',
            'table'    => 'mono_company_drive_users',
            'key'      => 'drive_id',
            'otherKey' => 'user_id',
        ]
    ];

    public $attachOne = [
        'thumbnail' => ['System\Models\File', 'public' => true]
    ];

    /**
     * Builds the S3 client from the supplied settings
     * @return S3Client
     */
    private function getS3Client() {
        $client = new S3Client([
            'credentials' => [
                'key'    => Settings::get('key'),
                'secret' => Settings::get('secret')
            ],
            'region' => Settings::get('region'),
            'version' => '2006-03-01',
        ]);
        return $client;        
    }

    /**
     * Builds Flysystem adapter
     * @return Filesystem
     */
    private function getFilesystem() {
        $adapter = new AwsS3Adapter($this->getS3Client(), Settings::get('bucket'));
        return new Filesystem($adapter, [
            'visibility' => AdapterInterface::VISIBILITY_PRIVATE
        ]);
    }

    /**
     * Before Save, make sure that our paths are correct
     * If we're saving a directory, take care of actually
     * creating it on S3 here
     * We don't handle File creation here as that's a separate
     * upload process
     */
    public function beforeSave() {

        $filesystem = $this->getFilesystem();

        if ($this->type == 'file') {
            // add trailing / to path
            if (substr($this->path, -1) != '/') $this->path .= '/';
        }

        if ($this->type == 'directory') {
            // remove trailing /
            $this->name = rtrim($this->name, '/');
            $filesystem->createDir($this->name);
            $this->path = $this->name;
        }

    }

    /**
     * Before deleting a file, take care of actually deleting it from S3
     */
    public function beforeDelete() {

        $filesystem = $this->getFileSystem();
        if ($this->type == 'file') {
            if ($filesystem->has($this->filePath())) {
                $filesystem->delete($this->filePath());
            }
        }

        else if ($this->type == 'directory') {
            $contents = $filesystem->listContents($this->path, true);
            // don't delete non-empty directories
            if ($contents) throw new \Exception ("Directory not empty");
            $filesystem->deleteDir($this->path);
        }

    }

    /**
     * Writes a file via stream upload to S3
     */
    public function writeFile($stream) {
        $filesystem = $this->getFileSystem();
        // create or update
        $filesystem->putStream($this->filePath(), $stream);
    }

    /**
     * Retrieves all contents from S3
     */
    public function getAll() {
        $filesystem = $this->getFileSystem();
        return $filesystem->listContents('/', true);
    }

    /**
     * Retrieves a specific file from S3
     * @param string optional version id
     * @return Response stream of file
     */
    public function retrieve($version_id = false) {

        // if a version id has been supplied, we want to handle this
        // slightly differently as FlySystem doesn't support
        // retrieving versions
        if ($version_id) return $this->retrieveByVersion($version_id);

        $filesystem = $this->getFileSystem();
        $stream     = $filesystem->readStream($this->filePath());

        $headers    = [
            "Content-Type"        => $filesystem->getMimetype($this->path),
            "Content-Length"      => $filesystem->getSize($this->path),
            "Content-disposition" => "attachment; filename=". $this->name,
        ];

        return Response::stream(
            function() use($stream) {
                fpassthru($stream);
            },
            200,
            $headers
        );

    }

    /**
     * Lists the versions of the file stored at S3
     * @return array of versions
     */
    public function getVersions() {
        $client = $this->getS3Client();
        // so apparently there isn't a way to get versions of a specific object
        // so we'll have to get all version information then iterate over it
        $versions = $client->listObjectVersions([
            'Bucket'    => Settings::get('bucket'),
            //'KeyMarker' => $this->filePath(),
            //'MaxKeys'   => 1
        ]);
        $array = [];
        foreach ($versions->get("Versions") as $version) {
            if ($version['Key'] == $this->filePath()) {
                $array[] = $version;
            }
        }
        return $array;
    }

    /**
     * Retrieve a file from S3 by it's Version ID
     * FlySystem doesn't support this so we have to 
     * use the regular S3Client to do it
     * @param string Version ID
     * @return stream echo
     */
    public function retrieveByVersion($version_id) {

        $client   = $this->getS3Client();
        $response = $client->getObject([
            'Bucket'    => Settings::get('bucket'),
            'Key'       => $this->filePath(),
            'VersionId' => $version_id,
        ]);

        $stream = $response->get('Body');
        $mime   = $response->get('ContentType');
        // give this version a different name for the download
        $name   = str_slug('Version ' . $response->get('LastModified')->format('Y-m-d H-i-s') . ' ' . $this->name);

        header("Content-Type: {$mime}" );
        header("Content-Disposition: attachment; filename=$name" );
        header('Pragma: public');
        echo $stream;
        die();

    }

    /**
     * Generates a full file path (/directory/filename.extension)
     * from the path and name fields
     * @return string full path
     */
    private function filePath() {
        $path = $this->path;
        if (substr($path, -1) != '/') $path .= '/';
        $path .= $this->name;
        return $path;
    }

}

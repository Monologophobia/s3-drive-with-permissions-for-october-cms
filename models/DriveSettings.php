<?php namespace Monologophobia\Drive\Models;

use Model;

/**
 * Store System Default Payment Settings
 * for use by all modules
 */
class DriveSettings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'mono_company_drive_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}

<?php namespace Monologophobia\Drive\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class Drive extends Migration {

    public function up() {

        Schema::create('mono_company_drive', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('type')->default('file');
            $table->integer('owner_id')->unsigned()->nullable();
            $table->foreign('owner_id')->references('id')->on('backend_users')->onDelete('SET NULL');
            $table->text('path');
            $table->string('mime')->nullable();
            $table->timestamps();
        });

        Schema::create('mono_company_drive_users', function($table) {
            $table->integer('drive_id')->index()->unsigned();
            $table->foreign('drive_id')->references('id')->on('mono_company_drive')->onDelete('cascade');
            $table->integer('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('backend_users')->onDelete('cascade');
        });

    }

    public function down() {

        Schema::dropIfExists('mono_company_drive_users');
        Schema::dropIfExists('mono_company_drive');

    }


}

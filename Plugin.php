<?php namespace Monologophobia\Drive;

use Log;
use Lang;
use Mail;
use Session;
use Event as SystemEvent;

use Backend\Facades\Backend;
use Backend\Models\UserGroup;
use System\Classes\PluginBase;

class Plugin extends PluginBase {

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Drive',
            'author'      => 'Monologophobia',
            'icon'        => 'icon-files-o',
        ];
    }

    /**
     * Register plugin permissions
     *
     * @return array
     */
    public function registerPermissions() {
        return [
            'monologophobia.drive.drive_admin' => ['tab' => 'Drive', 'label' => 'Can read and delete all Drive files'],
        ];
    }

    public function registerSettings() {
        return [
            'drive_settings' => [
                'label'       => 'Drive Settings',
                'description' => 'Manage Drive Settings',
                'category'    => 'Gym',
                'icon'        => 'icon-files-o',
                'class'       => 'Monologophobia\Drive\Models\DriveSettings',
                'order'       => 500,
                'permissions' => ['monologophobia.drive.drive_admin']
            ]
        ];
    }


    // Create the backend navigation
    public function registerNavigation() {
        return [
            'drive' => [
                'label' => 'Drive',
                'url'   => Backend::url('monologophobia/drive/drive'),
                'icon'  => 'icon-files-o'
            ]
        ];
    }

    public function boot() {

        // extend backend user to include drive files
        \Backend\Models\User::extend(function($model) {
            $model->hasMany['own_files'] = [
                'Monologophobia\Drive\Models\Drive',
                'key' => 'owner_id'
            ];
            $model->belongsToMany['files'] = [
                'Monologophobia\Drive\Models\Drive',
                'table'    => 'mono_company_drive_users',
                'key'      => 'user_id',
                'otherKey' => 'drive_id',
            ];
        });

    }

    public function registerSchedule($schedule) {

        $schedule->call(function() {
            //$this->sendReportEmails();
        })->monthly();

    }

}

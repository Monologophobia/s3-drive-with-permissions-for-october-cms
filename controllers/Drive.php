<?php namespace Monologophobia\Drive\Controllers;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

use October\Rain\Database\Attach\Resizer;

use Flash;
use Input;
use Backend;
use Response;
use BackendAuth;
use BackendMenu;
use Backend\Models\User;
use Backend\Classes\Controller;

use Monologophobia\Drive\Models\Drive as DriveModel;

/**
 * Controls the Drive system
 */
class Drive extends Controller {

    protected $user;

    public function __construct() {

        parent::__construct();

        BackendMenu::setContext('Monologophobia.Drive', 'drive', 'drive');
        $this->pageTitle = 'Drive';

        $this->user  = BackendAuth::getUser();

    }

    /**
     * Load required Javascript and CSS
     * If the user is a superuser or has the admin permission then load all items
     * Otherwise load only the relations of the currently logged in user
     */
    public function index() {

        $this->bodyClass = 'compact-container';
        $this->addCss('/plugins/monologophobia/drive/assets/css/drive.css');
        $this->addCss('/plugins/monologophobia/drive/assets/css/dropzone.css');
        $this->addJs('/plugins/monologophobia/drive/assets/js/dropzone.js');

        $this->vars['user']  = $this->user;
        $this->vars['users'] = User::get();
        $this->vars['superuser'] = ($this->user->is_superuser || $this->user->hasPermission(['monologophobia.drive.drive_admin']));

        if ($this->user->is_superuser || $this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            $directories = DriveModel::where('type', 'directory')->get();
            $this->vars['directories'] = $directories;
        }
        else {
            $items = $this->user->own_files()->with('thumbnail')->where('type', 'directory')->get();
            $items = $items->merge($this->user->files()->with('thumbnail')->where('type', 'directory')->get());
            // because we're loading 2 different relationships we could have a collision of the same items
            $items = $items->unique();
            $directories = [];
            foreach ($items as $item) {
                if ($item->type == 'directory') $directories[] = $item;
            }
            $this->vars['directories'] = $directories;
        }

    }

    /**
     * Load Directory contents from a supplied directory path
     * If superuser or similar, return all items
     * If not, return relationships
     * @notquiteparam POST string directory
     * @return Collection of DriveModel
     */
    public function onGetDirectoryContents() {
        $directory = post('directory');
        if (substr($directory, -1) != '/') $directory .= '/';
        if ($this->user->is_superuser || $this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            return DriveModel::where('type', 'file')->where('path', $directory)->with('users')->with('thumbnail')->get();
        }
        else {
            $items = $this->user->own_files()->with('thumbnail')->where('type', 'file')->where('path', $directory)->with('users')->get();
            $items = $items->merge($this->user->files()->with('thumbnail')->where('type', 'file')->where('path', $directory)->with('users')->get());
            $items = $items->unique();
            return $items;
        }
    }

    /**
     * Search the Drive database for a name field LIKE supplied parameter
     * Escapes the search term as Laravel does parameterise the query but
     * it's possible the user enters a wildcard character that could
     * have some nasty performance consequences on the database
     * @notquiteparam string search term
     * @return Collection of DriveModel
     */
    public function onSearch() {
        $search = '%' . post('search') . '%';
        $search = $this->escape_like($search);
        if ($this->user->is_superuser || $this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            return DriveModel::where('type', 'file')->where('name', 'like', $search)->with('users')->with('thumbnail')->get();
        }
        else {
            $items = $this->user->own_files()->with('users')->with('thumbnail')->where('type', 'file')
                                             ->where('name', 'like', $search)->get();
            $items = $items->merge($this->user->files()->with('users')->with('thumbnail')->where('type', 'file')
                                                       ->where('name', 'like', $search)->get());
            return $items;
        }
    }

    /**
    * Escape special characters for a LIKE query.
    * Taken from https://stackoverflow.com/questions/22749182/laravel-escape-like-clause/42028380#42028380
    *
    * @param string $value
    * @param string $char
    *
    * @return string
    */
    private function escape_like(string $value, string $char = '\\'): string {
        return str_replace(
            [$char, '%', '_'],
            [$char.$char, $char.'%', $char.'_'],
            $value
        );
    }

    /**
     * Check each item that is physically on the drive to see
     * if we have a corresponding permission record for it
     * if not, return it as an orphaned record
     * TODO: This
     */
    private function mergeOrphaned($items, $drive_contents) {
        foreach ($items as $item) {
            foreach ($drive_contents as $key => $content) {
                if (($item->type == 'directory' && $content['type'] == 'dir') || $item->type == 'file' && $content['type'] == 'file') {
                    if ($item->path == $content['path']) {
                        unset($drive_contents[$key]);
                    }
                }
            }
        }
        // rebase indexes
        $drive_contents = array_values($drive_contents);
        // create orphaned drive items
        dd($contents);
    }

    /**
     * Upload a file
     * Does not support multiple uploads, do them one at a time
     * Currently using dropzone to send the files from the front end but
     * any request that sends POST file_data as a file will work
     * @notquiteparam POST file file_data
     * @return DriveModel item that was uploaded
     */
    public function upload() {

        if (!Input::hasFile('file_data')) {
            throw new \ApplicationException('File missing from request');
        }

        $file = Input::file('file_data');

        $name = $file->getClientOriginalName();
        // we want to trim off any trailing / as the Model takes care of it
        $path = rtrim(post('directory_name'), '/');
        // create or update
        $item           = DriveModel::where('path', $path . '/')->where('name', $name)->first();
        if (!$item) $item = new DriveModel;
        $item->type     = 'file';
        $item->owner_id = $this->user->id;
        $item->name     = $name;
        $item->path     = $path;
        $item->mime     = $file->getMimeType();
        $item->updated_at = date('Y-m-d H:i:s');

        // Write the file to S3
        $stream = fopen($file->getRealPath(), 'r+');
        $item->writeFile($stream);
        fclose($stream);

        $item->save();

        // if this is an image, generate a thumbnail
        // @ suppresses exceptions apparently...
        if (@is_array(getimagesize($file->getRealPath()))) {
            try {

                // create a temporary thumbnail
                $thumbnail = storage_path('app/uploads/public/' . $path);
                Resizer::open($file)->resize(300, 300, ['crop'])->save($thumbnail, 60);

                $item->thumbnail()->create(['data' => $thumbnail]);
                
                // delete the temporary file
                \File::delete($thumbnail);

            }
            catch (\Exception $e) {
                \Log::error($e);
            }
        }

        // add permissions
        $user_ids = post('user_id');
        if (!is_array($user_ids)) $user_ids = explode(',', $user_ids);

        $item->users()->sync($user_ids);

        return DriveModel::with('users')->with('thumbnail')->findOrFail($item->id);

    }

    /**
     * Deletes a file or directory
     * Supports multiples sent as an array of IDs
     * @notquiteparam int or array of ints ID to delete
     * @return Array of ['deleted' => Collection of items]
     */
    public function onDelete() {
        try {
            $id = post('id');
            $return = [];
            if (is_array($id)) {
                // map the incoming array to intvals
                $ids = array_map('intval', $id);
                $items = DriveModel::whereIn('id', $ids)->get();
                foreach ($items as $item) {
                    // store the deleted items to return to UI
                    $return[] = $item;
                    $this->deleteItem($item);
                }
            }
            else {
                $item = DriveModel::where('id', intval($id))->firstOrFail();
                $return[] = $item;
                $this->deleteItem($item);
            }
            return ['deleted' => $return];
        }
        catch (\Exception $e) {
            throw new \AjaxException($e->getMessage());
        }
    }

    /**
     * Do the actual deleting coming from onDelete
     * If superuser or similar, actually delete the item
     * If owner of item, delete the item
     * If regular user, just remove their permission on the item
     * @param DriveModel
     * @return DriveModel that was deleted
     */
    private function deleteItem($item) {
        if ($this->user->is_superuser || $this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            $item->delete();
        }
        else {
            if ($item->owner_id == $this->user->id) {
                $item->delete();
            }
            else {
                // check if this is just a permission allow, if so remove permission
                if ($item->users()->where('user_id', $this->user->id)->exists()) {
                    $this->user->files()->detach($item);
                }
            }
        }
        return $item;
    }

    /**
     * Create a directory and add permissions
     * @notquiteparam POST string directory_name
     * @return DriveModel
     */
    public function onCreateDirectory() {
        try {

            $name = filter_var(post('directory_name'), FILTER_SANITIZE_STRING);

            $item = new DriveModel;
            $item->name = $name;
            $item->type = 'directory';
            $item->owner_id = $this->user->id;
            $item->save();

            $user_ids = post('user_id');
            if (!is_array($user_ids) || !count($user_ids)) {
                $user_ids = [];
            }
            else {
                // map to intvals
                $user_ids = array_map('intval', $user_ids);
            }
            
            $item->users()->sync($user_ids);

            return DriveModel::with('users')->findOrFail($item->id);

        }
        catch (\Exception $e) {
            throw new \AjaxException($e->getMessage());
        }
    }

    /**
     * Update permissions of items with selected ids with selected userids
     * @notquiteparam POST int or array of ints DriveModel IDs
     * @notquiteparam POST array of ints User IDs
     */
    public function onUpdatePermissions() {
        try {

            $id       = post('id');
            $user_ids = post('user_id');
            if (!is_array($user_ids) || !count($user_ids)) {
                $user_ids = [];
            }
            // make sure they're numeric
            $user_ids = array_map('intval', $user_ids);

            // if multiple items, iterate
            if (is_array($id)) {
                $ids = array_map('intval', $id);
                foreach ($ids as $id) {
                    // If the user is a superuser or similar or owns the item then
                    // allow the permissions change
                    if (
                        $this->user->is_superuser ||
                        $this->user->hasPermission(['monologophobia.drive.drive_admin']) ||
                        $item->owner_id == $this->user->id
                    ) {
                        $item = DriveModel::find($id);
                        if ($item) {
                            $item->users()->sync($user_ids);
                        }
                    }
                }
            }
            // else single item
            else {

                $item = DriveModel::findOrFail(intval($id));

                // check if superuser or owner, if not disallow
                if (!$this->user->is_superuser && !$this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
                    if ($item->owner_id != $this->user->id) {
                        throw new \Exception("You do not own this item");
                    }
                }

                $item->users()->sync($user_ids);

            }

            Flash::success('Permissions updated');

        }
        catch (\Exception $e) {
            throw new \AjaxException($e->getMessage());
        }
    }

    /**
     * Changes files ownership
     * @notquiteparam int of DriveModel ID
     * @return DriveModel with new ownership
     */
    public function onChangeOwner() {
        $item = DriveModel::findOrFail(intval(post('id')));
        // check superuser or owner
        if (!$this->user->is_superuser && !$this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            if ($item->owner_id != $this->user->id) {
                throw new \AjaxException("You do not own this item");
            }
        }
        $item->owner_id = intval(post('owner_id'));
        $item->save();
        return DriveModel::with('users')->with('thumbnail')->findOrFail($item->id);
    }

    /**
     * Get versions of file from S3
     * @notquiteparam int of DriveModel ID
     * @return array ['versions' => array of versions]
     */
    public function onGetVersions() {
        $item = DriveModel::findOrFail(intval(post('id')));
        $versions = (array) $item->getVersions();
        return ['versions' => $versions];
        die();
    }

    /**
     * Downloads a file from S3
     * Retrieves the file from the bucket and streams to the browser
     * Also supports downloading a specific version of the item
     * @param int ID of DriveModel
     * @param string optional version ID
     * @return stream of file
     */
    public function retrieve(int $id, $version_id = false) {
        $item = DriveModel::findOrFail($id);
        // check permission to access this file
        if (!$this->user->is_superuser || !$this->user->hasPermission(['monologophobia.drive.drive_admin'])) {
            if ($item->owner_id != $this->user->id) {
                if (!$item->users()->where('user_id', $this->user->id)->exists()) {
                    throw new \Exception("You do not have access to this item");
                }
            }
        }
        return $item->retrieve($version_id);
        die();
    }

}
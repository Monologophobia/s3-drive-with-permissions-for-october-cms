<?php

use \Monologophobia\Drive\Models\Drive as DriveModel;
use BackendAuth;

Route::post('api/drive/login', function () {
    $user  = $this->checkCredentials();
    if ($user->is_superuser || $user->hasPermission(['monologophobia.drive.drive_admin'])) {
        return DriveModel::get();
    }
    else {
        $items = $this->user->own_files;
        $items = $items->merge($this->user->files);
        // because we're loading 2 different relationships we could have a collision of the same items
        $items = $items->unique();
        return $items;
    }
});

function checkCredentials() {
    $credentials = [
        'email'    => filter_var(post('email'), FILTER_SANITIZE_EMAIL),
        'password' => post('password')
    ];
    return BackendAuth::attempt($credentials);
}